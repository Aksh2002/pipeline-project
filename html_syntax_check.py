import os
import subprocess
from pathlib import Path

def check_html_syntax(project_path):
    # List HTML files
    html_files = [file for file in os.listdir(project_path) if file.endswith('.html')]

    for file in html_files:
        file_path = os.path.join(project_path, file)
        try:
            # Use subprocess to call the html5validator command-line interface
            subprocess.run(['html5validator', file_path], check=True)
            print(f"HTML syntax is valid in {file_path}")
        except subprocess.CalledProcessError as e:
            print(f"HTML syntax error in {file_path}: {e}")

def main():
    project_path = Path(__file__).resolve().parent
    check_html_syntax(project_path)

if __name__ == "__main__":
    main()